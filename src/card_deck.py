from collections import namedtuple

Card = namedtuple("Card", ["rank", "suit"])


class FrenchDeck:
    __ranks = [str(n) for n in range(2, 11)] + list("JQKA")
    __suits = "spades diamonds hearts clubs".split()

    def __init__(self):
        self.__cards = [Card(rank, suit) for rank in self.__ranks
                        for suit in self.__suits]

    def __len__(self):
        return len(self.__cards)

    def __getitem__(self, position):
        return self.__cards[position]

    def spades_high(self, card):
        suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0)
        rank_value = self.__ranks.index(card.rank)
        return (rank_value * len(suit_values)) + suit_values[card.suit]
