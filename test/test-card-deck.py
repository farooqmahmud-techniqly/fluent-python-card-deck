import unittest
from src.card_deck import FrenchDeck, Card


class TestCardDeck(unittest.TestCase):
    def setUp(self):
        self.__deck = FrenchDeck()

    def test_deck_has_52_cards(self):
        self.assertEqual(52, len(self.__deck))

    def test_get_card(self):
        expected_card = Card("2", "spades")
        self.assertEqual(expected_card, self.__deck[0])

    def test_spades_high_returns_highest_card(self):
        sorted_deck = sorted(self.__deck, key=self.__deck.spades_high)
        self.assertEqual(Card("2", "clubs"), sorted_deck[0])
        self.assertEqual(Card("A", "spades"), sorted_deck[-1])


if __name__ == "__main__":
    unittest.main()
